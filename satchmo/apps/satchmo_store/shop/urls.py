from satchmo_utils.django_compat import include, patterns, url
from django.views.generic.base import TemplateView
from product.urls import urlpatterns as productpatterns
from satchmo_store import shop
from satchmo_store.shop.views.sitemaps import sitemaps
from satchmo_utils.signals import collect_urls

import django.contrib.sitemaps.views
import satchmo_store.shop.views
import satchmo_store.shop.views.contact
import satchmo_store.shop.views.home
import satchmo_store.shop.views.orders
import satchmo_store.shop.views.search
import satchmo_store.shop.views.smart

urlpatterns = shop.get_satchmo_setting('SHOP_URLS')

urlpatterns += patterns('',
    url(r'^$', satchmo_store.shop.views.home.home, {}, 'satchmo_shop_home'),
    url(r'^add/$', satchmo_store.shop.views.smart.smart_add, {}, 'satchmo_smart_add'),
    url(r'^cart/$', satchmo_store.shop.views.cart.display, {}, 'satchmo_cart'),
    url(r'^cart/accept/$', satchmo_store.shop.views.cart.agree_terms, {}, 'satchmo_cart_accept_terms'),
    url(r'^cart/add/$', satchmo_store.shop.views.cart.add, {}, 'satchmo_cart_add'),
    url(r'^cart/add/ajax/$', satchmo_store.shop.views.cart.add_ajax, {}, 'satchmo_cart_add_ajax'),
    url(r'^cart/qty/$', satchmo_store.shop.views.cart.set_quantity, {}, 'satchmo_cart_set_qty'),
    url(r'^cart/qty/ajax/$', satchmo_store.shop.views.cart.set_quantity_ajax, {}, 'satchmo_cart_set_qty_ajax'),
    url(r'^cart/remove/$', satchmo_store.shop.views.cart.remove, {}, 'satchmo_cart_remove'),
    url(r'^cart/remove/ajax/$', satchmo_store.shop.views.cart.remove_ajax, {}, 'satchmo_cart_remove_ajax'),
    url(r'^checkout/', include('payment.urls')),
    url(r'^contact/$', satchmo_store.shop.views.contact.form, {}, 'satchmo_contact'),
    url(r'^history/$', satchmo_store.shop.views.orders.order_history, {}, 'satchmo_order_history'),
    url(r'^quickorder/$', satchmo_store.shop.views.cart.add_multiple, {}, 'satchmo_quick_order'),
    url(r'^tracking/(?P<order_id>\d+)/$', satchmo_store.shop.views.orders.order_tracking, {}, 'satchmo_order_tracking'),
    url(r'^search/$', satchmo_store.shop.views.search.search_view, {}, 'satchmo_search'),
)

# here we add product patterns directly into the root url
urlpatterns += productpatterns

urlpatterns += patterns('',
    url(r'^contact/thankyou/$',
        TemplateView.as_view(template_name='shop/contact_thanks.html'), {},
        'satchmo_contact_thanks'),
    url(r'^sitemap\.xml$', django.contrib.sitemaps.views.sitemap,
        {'sitemaps': sitemaps},
        'satchmo_sitemap_xml'),

)

# here we are sending a signal to add patterns to the base of the shop.
collect_urls.send(sender=shop, patterns=urlpatterns)
