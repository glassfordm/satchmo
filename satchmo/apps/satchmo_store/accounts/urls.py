"""
URLConf for Django user registration.

Recommended usage is to use a call to ``include()`` in your project's
root URLConf to include this URLConf for any URL beginning with
'/accounts/'.

"""
from satchmo_utils.django_compat import patterns, url

from satchmo_store.accounts.views import RegistrationComplete
# extending the urls in contacts
from satchmo_store.contact.urls import urlpatterns
from satchmo_utils.signals import collect_urls
from satchmo_store import accounts
# The following import of satchmo_store.contact.config should not be removed
# because it is sometimes indirectly important for loading config_value('SHOP', 'ACCOUNT_VERIFICATION')
import satchmo_store.contact.config

import django.contrib.auth.views
import satchmo_store.accounts.views


# Activation keys get matched by \w+ instead of the more specific
# [a-fA-F0-9]+ because a bad activation key should still get to the view;
# that way it can return a sensible "invalid key" message instead of a
# confusing 404.
urlpatterns += patterns('',
    url(r'^activate/(?P<activation_key>\w+)/$', satchmo_store.accounts.views.activate, {}, 'registration_activate'),
    url(r'^login/$', satchmo_store.accounts.views.emaillogin, {'template_name': 'registration/login.html'}, 'auth_login'),
    url(r'^register/$', satchmo_store.accounts.views.register, {}, 'registration_register'),
    url(r'^secure/login/$', satchmo_store.accounts.views.emaillogin, {'SSL' : True, 'template_name': 'registration/login.html'}, 'auth_secure_login'),
)

urlpatterns += patterns('',
    url('^logout/$', django.contrib.auth.views.logout, {'template_name': 'registration/logout.html'}, 'auth_logout'),)

urlpatterns += patterns('',
    url(r'^register/complete/$',
        RegistrationComplete.as_view(template_name='registration/registration_complete.html'), {},
        'registration_complete'),
)

#Dictionary for authentication views
password_reset_dict = {
    'template_name': 'registration/password_reset_form.html',
    'email_template_name': 'registration/password_reset.txt',
}

# the "from email" in password reset is problematic... it is hard coded as None
urlpatterns += patterns('',
    url(r'^password_reset/$', django.contrib.auth.views.password_reset, password_reset_dict, 'auth_password_reset'),
    url(r'^password_reset/done/$', django.contrib.auth.views.password_reset_done, {'template_name':'registration/password_reset_done.html'}, 'auth_password_reset_done'),
    url(r'^password_change/$', django.contrib.auth.views.password_change, {'template_name':'registration/password_change_form.html'}, 'auth_password_change'),
    url(r'^password_change/done/$', django.contrib.auth.views.password_change_done, {'template_name':'registration/password_change_done.html'}, 'auth_change_done'),
    url(r'^reset/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$', django.contrib.auth.views.password_reset_confirm),
    url(r'^reset/done/$', django.contrib.auth.views.password_reset_complete),
)

collect_urls.send(sender=accounts, patterns=urlpatterns)
