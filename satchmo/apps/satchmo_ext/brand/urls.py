"""
Urls for Product Brand module, note that you do not have to add this to your urls file, it will get automatically included by collect_urls.
"""
from satchmo_utils.django_compat import include, patterns, url
from satchmo_store.shop import get_satchmo_setting
import logging

import satchmo_ext.brand.views

log = logging.getLogger('brand.urls')

urlpatterns = patterns('satchmo_ext.brand.views',
    url(r'^$', satchmo_ext.brand.views.brand_list, {}, 'satchmo_brand_list'),
    url(r'^(?P<brandname>.*)/(?P<catname>.*)/$', satchmo_ext.brand.views.brand_category_page, {}, 'satchmo_brand_category_view'),
    url(r'^(?P<brandname>.*)/$', satchmo_ext.brand.views.brand_page, {}, 'satchmo_brand_view'),
)

brandbase = r'^' + get_satchmo_setting('BRAND_SLUG') + '/'

prodbase = r'^' + get_satchmo_setting('PRODUCT_SLUG') + '/'
brandpatterns = patterns('',
    url(brandbase, include('satchmo_ext.brand.urls'))
)

def add_brand_urls(sender, patterns=(), section="", **kwargs):
    if section=="__init__":
        log.debug('adding brand urls at %s', brandbase)
        patterns += brandpatterns
