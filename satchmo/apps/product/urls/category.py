from satchmo_utils.django_compat import patterns, url

import product.views

urlpatterns = patterns('',
    url(r'^(?P<parent_slugs>([-\w]+/)*)?(?P<slug>[-\w]+)/$',
        product.views.category_view, {}, 'satchmo_category'),
    url(r'^$', product.views.category_index, {}, 'satchmo_category_index'),
)
