# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0003_auto_20160727_0758'),
    ]

    operations = [
        migrations.RenameField(
            model_name='category',
            old_name='sites',
            new_name='site',
        ),
        migrations.RenameField(
            model_name='discount',
            old_name='sites',
            new_name='site',
        ),
        migrations.RenameField(
            model_name='optiongroup',
            old_name='sites',
            new_name='site',
        ),
        migrations.RenameField(
            model_name='product',
            old_name='sites',
            new_name='site',
        ),
    ]
