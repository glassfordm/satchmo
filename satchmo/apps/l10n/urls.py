from satchmo_utils.django_compat import patterns, url

urlpatterns = patterns('',
    url(r'^setlang/$', 'django.views.i18n.set_language', {}, 'satchmo_set_language'),
)
