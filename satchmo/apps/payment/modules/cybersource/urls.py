from satchmo_utils.django_compat import patterns, url
from satchmo_store.shop.satchmo_settings import get_satchmo_setting

import payment.modules.cybersource.views
import payment.views.checkout

ssl = get_satchmo_setting('SSL', default_value=False)

urlpatterns = patterns('',
     url(r'^$', payment.modules.cybersource.views.pay_ship_info, {'SSL':ssl}, 'CYBERSOURCE_satchmo_checkout-step2'),
     url(r'^confirm/$', payment.modules.cybersource.views.confirm_info, {'SSL':ssl}, 'CYBERSOURCE_satchmo_checkout-step3'),
     url(r'^success/$', payment.views.checkout.success, {'SSL':ssl}, 'CYBERSOURCE_satchmo_checkout-success'),
)
