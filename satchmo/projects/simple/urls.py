from satchmo_utils.django_compat import patterns, url

from satchmo_store.urls import urlpatterns

urlpatterns += patterns('',
    url(r'test/', include('simple.localsite.urls'))
)
