from satchmo_utils.django_compat import patterns, url
from satchmo_store.shop.satchmo_settings import get_satchmo_setting

import payment.modules.payflowpro.views
import payment.views.checkout.success

ssl = get_satchmo_setting('SSL', default_value=False)

urlpatterns = patterns('',
     url(r'^$', payment.modules.payflowpro.views.pay_ship_info, {'SSL':ssl},
      'PAYFLOWPRO_satchmo_checkout-step2'),
     url(r'^confirm/$', payment.modules.payflowpro.views.confirm_info,
      {'SSL':ssl}, 'PAYFLOWPRO_satchmo_checkout-step3'),
     url(r'^success/$', payment.views.checkout.success, {'SSL':ssl},
      'PAYFLOWPRO_satchmo_checkout-success'),
)
