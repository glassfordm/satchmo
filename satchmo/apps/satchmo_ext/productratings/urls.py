"""urlpatterns for productratings.  Note that you do not need to add these to your urls anywhere, they'll be automatically added by the collect_urls signals."""

from satchmo_utils.django_compat import include, patterns, url
import logging
import satchmo_ext.productratings.views

log = logging.getLogger('productratings.urls')

productpatterns = patterns('',
    (r'^view/bestrated/$', 
        satchmo_ext.productratings.views.display_bestratings, {}, 'satchmo_product_best_rated'),
)

# Override comments with our redirecting view. You can remove the next two
# URLs if you aren't using ratings.
#(r'^comments/post/$', 'comments.post_rating', {'maxcomments': 1 }, 'satchmo_rating_post'),
try:
    from django.contrib.comments.models import Comment
    comment_urls = 'django.contrib.comments.urls'
except ImportError:
    comment_urls = 'django_comments.urls'
commentpatterns = patterns('',
    url(r'^comments/', include(comment_urls)),
)

def add_product_urls(sender, patterns=(), section="", **kwargs):
    if section=="product":
        log.debug('adding ratings urls')
        patterns += productpatterns

def add_comment_urls(sender, patterns=(), **kwargs):
    log.debug('adding comments urls')
    patterns += commentpatterns
