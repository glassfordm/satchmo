"""
Urls for wishlists, note that this does not have to get added manually to the urls, it will be added automatically by satchmo core if this app is installed.
"""
from satchmo_utils.django_compat import patterns, include, url
from livesettings.functions import config_value
import logging

import satchmo_ext.wishlist.views

log = logging.getLogger('wishlist.urls')

urlpatterns = patterns('',
     url(r'^$', satchmo_ext.wishlist.views.wishlist_view, {}, 'satchmo_wishlist_view'),
     url(r'^add/$', satchmo_ext.wishlist.views.wishlist_add, {}, 'satchmo_wishlist_add'),
     url(r'^add/ajax/$', satchmo_ext.wishlist.views.wishlist_add_ajax, {}, 'satchmo_wishlist_add_ajax'),
     url(r'^remove/$', satchmo_ext.wishlist.views.wishlist_remove, {}, 'satchmo_wishlist_remove'),
     url(r'^remove/ajax$', satchmo_ext.wishlist.views.wishlist_remove_ajax, {}, 'satchmo_wishlist_remove_ajax'),
     url(r'^add_cart/$', satchmo_ext.wishlist.views.wishlist_move_to_cart, {}, 'satchmo_wishlist_move_to_cart'),
)
patterns_fn = patterns

def add_wishlist_urls(sender, patterns=(), **kwargs):
    wishbase = r'^' + config_value('SHOP','WISHLIST_SLUG') + '/'    
    log.debug('adding wishlist urls at %s', wishbase)
    wishpatterns = patterns_fn('',
        url(wishbase, include('satchmo_ext.wishlist.urls'))
    )
    patterns += wishpatterns
