"""Urls which need to be loaded at root level."""
from satchmo_utils.django_compat import patterns, url

import product.views
import product.views.adminviews

adminpatterns = patterns('',
    url(r'^admin/product/configurableproduct/(?P<id>\d+)/getoptions/', 
        product.views.get_configurable_product_options, {}, 
        'satchmo_admin_configurableproduct'),
)

adminpatterns += patterns('',
    url(r'^admin/inventory/edit/$', 
        product.views.adminviews.edit_inventory, {}, 'satchmo_admin_edit_inventory'),
    url(r'^inventory/export/$',
        product.views.adminviews.export_products, {}, 'satchmo_admin_product_export'),
    url(r'^inventory/import/$', 
        product.views.adminviews.import_products, {}, 'satchmo_admin_product_import'),
    # url(r'^inventory/report/$', 
    #     product.views.adminviews.product_active_report, {}, 'satchmo_admin_product_report'),
    url(r'^admin/(?P<product_id>\d+)/variations/$', 
        product.views.adminviews.variation_manager, {}, 'satchmo_admin_variation_manager'),
    url(r'^admin/variations/$', 
        product.views.adminviews.variation_list, {}, 'satchmo_admin_variation_list'),
)
