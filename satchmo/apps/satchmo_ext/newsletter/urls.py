"""
URLConf for Satchmo Newsletter app

This will get automatically added by satchmo_store, under the url given in your livesettings "NEWSLETTER","NEWSLETTER_SLUG"
"""

from satchmo_utils.django_compat import include, patterns, url
from livesettings.functions import config_value
import logging
import satchmo_ext.newsletter.views


log = logging.getLogger('newsletter.urls')

urlpatterns = patterns('',
    url(r'^subscribe/$', satchmo_ext.newsletter.views.add_subscription, {}, 'newsletter_subscribe'),
    url(r'^subscribe/ajah/$', satchmo_ext.newsletter.views.add_subscription,
        {'result_template' : 'newsletter/ajah.html'}, 'newsletter_subscribe_ajah'),
    url(r'^unsubscribe/$', satchmo_ext.newsletter.views.remove_subscription, 
        {}, 'newsletter_unsubscribe'),
    url(r'^unsubscribe/ajah/$', satchmo_ext.newsletter.views.remove_subscription, 
        {'result_template' : 'newsletter/ajah.html'}, 'newsletter_unsubscribe_ajah'),
    url(r'^update/$', satchmo_ext.newsletter.views.update_subscription, {}, 'newsletter_update'),
)
patterns_fn = patterns

def add_newsletter_urls(sender, patterns=(), **kwargs):
    newsbase = r'^' + config_value('NEWSLETTER','NEWSLETTER_SLUG') + '/'    
    log.debug("Adding newsletter urls at %s", newsbase)
    newspatterns = patterns_fn('',
        url(newsbase, include('satchmo_ext.newsletter.urls'))
    )
    patterns += newspatterns
