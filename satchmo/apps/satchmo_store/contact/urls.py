"""
URLConf for Satchmo Contacts.
"""

from satchmo_utils.django_compat import patterns, url
from satchmo_utils.signals import collect_urls
from satchmo_store import contact
from satchmo_store.shop.satchmo_settings import get_satchmo_setting

import satchmo_store.contact.views

ssl = get_satchmo_setting('SSL', default_value=False)

urlpatterns = patterns('',
    url(r'^$', satchmo_store.contact.views.view, {}, 'satchmo_account_info'),
    url(r'^update/$', satchmo_store.contact.views.update, {}, 'satchmo_profile_update'),
    url(r'^address/create/$', satchmo_store.contact.views.address_create_edit,{},'satchmo_address_create'),
    url(r'^address/edit/(?P<id>\d+)/$', satchmo_store.contact.views.address_create_edit,{},'satchmo_address_edit'),
    url(r'^address/delete/(?P<id>\d+)/$', satchmo_store.contact.views.address_delete,{},'satchmo_address_delete'),
    url(r'^ajax_state/$', satchmo_store.contact.views.ajax_get_state, {'SSL': ssl}, 'satchmo_contact_ajax_state'),
)

collect_urls.send(sender=contact, patterns=urlpatterns)
