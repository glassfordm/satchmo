from satchmo_utils.django_compat import patterns, url
from satchmo_store.shop.satchmo_settings import get_satchmo_setting
ssl = get_satchmo_setting('SSL', default_value=False)

import payment.modules.autosuccess.views
import payment.views.checkout

urlpatterns = patterns('',
     url(r'^$', payment.modules.autosuccess.views.one_step, {'SSL': ssl}, 'AUTOSUCCESS_satchmo_checkout-step2'),
     url(r'^success/$', payment.views.checkout.success, {'SSL': ssl}, 'AUTOSUCCESS_satchmo_checkout-success'),
)
