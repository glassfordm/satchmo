from satchmo_utils.django_compat import patterns, url
import product
from satchmo_utils.signals import collect_urls

import product.views
import product.views.filters

urlpatterns = patterns('',
    url(r'^(?P<product_slug>[-\w]+)/$', 
        product.views.get_product, {}, 'satchmo_product'),
    url(r'^(?P<product_slug>[-\w]+)/prices/$', 
        product.views.get_price, {}, 'satchmo_product_prices'),
    url(r'^(?P<product_slug>[-\w]+)/price_detail/$', 
        product.views.get_price_detail, {}, 'satchmo_product_price_detail'),
)

urlpatterns += patterns('',
    url(r'^view/recent/$', 
        product.views.filters.display_recent, {}, 'satchmo_product_recently_added'),
    url(r'^view/bestsellers/$', 
        product.views.filters.display_bestsellers, {}, 'satchmo_product_best_selling'),
)

# here we are sending a signal to add patterns to the base of the shop.
collect_urls.send(sender=product, patterns=urlpatterns, section="product")
