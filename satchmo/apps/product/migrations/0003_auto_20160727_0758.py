# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0002_sites'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category',
            name='site',
        ),
        migrations.RemoveField(
            model_name='discount',
            name='site',
        ),
        migrations.RemoveField(
            model_name='optiongroup',
            name='site',
        ),
        migrations.RemoveField(
            model_name='product',
            name='site',
        ),
        migrations.AlterField(
            model_name='category',
            name='sites',
            field=models.ManyToManyField(related_name='categories', verbose_name='Sites', to='sites.Site'),
        ),
        migrations.AlterField(
            model_name='discount',
            name='sites',
            field=models.ManyToManyField(related_name='discounts', verbose_name='Sites', to='sites.Site'),
        ),
        migrations.AlterField(
            model_name='optiongroup',
            name='sites',
            field=models.ManyToManyField(related_name='option_groups', verbose_name='Sites', to='sites.Site'),
        ),
        migrations.AlterField(
            model_name='price',
            name='quantity',
            field=models.DecimalField(help_text='Use this price only for this quantity or higher', verbose_name='Discount Quantity', max_digits=18, decimal_places=6),
        ),
        migrations.AlterField(
            model_name='product',
            name='sites',
            field=models.ManyToManyField(related_name='products', verbose_name='Sites', to='sites.Site'),
        ),
    ]
