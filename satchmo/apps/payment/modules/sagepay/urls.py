from satchmo_utils.django_compat import patterns, url
from satchmo_store.shop.satchmo_settings import get_satchmo_setting

import payment.modules.sagepay.views
import payment.views.checkout

ssl = get_satchmo_setting('SSL', default_value=False)

urlpatterns = patterns('',
    url(r'^$', payment.modules.sagepay.views.pay_ship_info,
        {'SSL':ssl}, 'SAGEPAY_satchmo_checkout-step2'),

    url(r'^confirm/$', payment.modules.sagepay.views.confirm_info,
        {'SSL':ssl}, 'SAGEPAY_satchmo_checkout-step3'),

    url(r'^secure3d/$', payment.modules.sagepay.views.confirm_secure3d,
       {'SSL':ssl}, 'SAGEPAY_satchmo_checkout-secure3d'),

    url(r'^success/$', payment.views.checkout.success,
        {'SSL':ssl}, 'SAGEPAY_satchmo_checkout-success'),
)
