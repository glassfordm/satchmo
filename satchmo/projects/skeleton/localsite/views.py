from django.shortcuts import render

def example(request):
    ctx = {}
    return render(request, 'localsite/example.html', ctx)
