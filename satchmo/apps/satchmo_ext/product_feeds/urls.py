"""
Urls for product feeds, note that this does not have to get added manually to the urls, it will be added automatically by satchmo core if this app is installed.
"""
from satchmo_utils.django_compat import include, patterns, url
import logging
import satchmo_ext.product_feeds.views

log = logging.getLogger('product_feeds.urls')

urlpatterns = patterns('',
    url(r'atom/$', satchmo_ext.product_feeds.views.product_feed, {}, 'satchmo_atom_feed'),
    url(r'atom/(?P<category>([-\w])*)/$', satchmo_ext.product_feeds.views.product_feed, {}, 'satchmo_atom_category_feed'),
    url(r'products.csv$', satchmo_ext.product_feeds.views.admin_product_feed, {'template' : "product_feeds/product_feed.csv"}, 'satchmo_product_feed'),
)

feedpatterns = patterns('',
    url(r'^feed/', include('satchmo_ext.product_feeds.urls'))
)

def add_feed_urls(sender, patterns=(), **kwargs):
    log.debug("Adding product_feed urls")
    patterns += feedpatterns
