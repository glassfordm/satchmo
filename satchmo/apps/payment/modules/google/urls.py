from satchmo_utils.django_compat import patterns, url
from satchmo_store.shop.satchmo_settings import get_satchmo_setting

import payment.modules.google.views
import payment.views.confirm

ssl = get_satchmo_setting('SSL', default_value=False)

urlpatterns = patterns('',
     url(r'^$', payment.modules.google.views.pay_ship_info, {'SSL': ssl}, 'GOOGLE_satchmo_checkout-step2'),
     url(r'^confirm/$', payment.modules.google.views.confirm_info, {'SSL': ssl}, 'GOOGLE_satchmo_checkout-step3'),
     url(r'^success/$', payment.modules.google.views.success, {'SSL': ssl}, 'GOOGLE_satchmo_checkout-success'),
     url(r'^notification/$', payment.modules.google.views.notification, {'SSL': ssl},
        'GOOGLE_satchmo_checkout-notification'),
     url(r'^confirmorder/$', payment.views.confirm.confirm_free_order,
        {'SSL' : ssl, 'key' : 'GOOGLE'}, 'GOOGLE_satchmo_checkout_free-confirm')
)
