def get_apps():
    from django import VERSION as DJANGO_VERSION
    if DJANGO_VERSION < (1,9):
        from django.db import models
        return models.get_apps()
    else:
        from django.apps import apps
        from django.utils.module_loading import import_module
        modules = set()
        result = list()
        for model in apps.get_models():
            module = model.__module__
            if not module in modules:
                modules.add(module)
                result.append(import_module(module))
        return result