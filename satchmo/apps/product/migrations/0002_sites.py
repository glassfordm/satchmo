# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sites', '0001_initial'),
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='sites',
            field=models.ManyToManyField(to='sites.Site', verbose_name='Sites'),
        ),
        migrations.AddField(
            model_name='discount',
            name='sites',
            field=models.ManyToManyField(to='sites.Site', verbose_name='Sites'),
        ),
        migrations.AddField(
            model_name='optiongroup',
            name='sites',
            field=models.ManyToManyField(to='sites.Site', verbose_name='Sites'),
        ),
        migrations.AddField(
            model_name='product',
            name='sites',
            field=models.ManyToManyField(to='sites.Site', verbose_name='Sites'),
        ),
    ]
