def get_methods():
    from models import Carrier, Shipper
    return [Shipper(carrier) for carrier in Carrier.objects.filter(active=True).order_by('ordering')]
