from django import VERSION as DJANGO_VERSION
from django.conf.urls import include, url  # for export only
from django.conf.urls.static import static

if DJANGO_VERSION < (1,8):
    from django.conf.urls import patterns
else:
    def patterns(prefix, *args):
        if prefix:
            # If the code has been converted to use this function,
            # it should also have been converted to use functions
            # instead of strings for the view specification,
            # meaning that the prefix should have been eliminated
            raise Exception("Patterns prefix not allowed (found {})".format(prefix))
        return list(args)
