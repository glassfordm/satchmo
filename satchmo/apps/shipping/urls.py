from satchmo_utils.django_compat import patterns, url

import shipping.views

#Urls which need to be loaded at root level.
adminpatterns = patterns('',
    url(r'^admin/print/(?P<doc>[-\w]+)/(?P<id>\d+)', 
        shipping.views.displayDoc, {}, 
        'satchmo_print_shipping'),
)
