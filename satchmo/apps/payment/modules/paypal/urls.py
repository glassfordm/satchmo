from satchmo_utils.django_compat import patterns, url
from satchmo_store.shop.satchmo_settings import get_satchmo_setting

import payment.modules.paypal.views
import payment.views.confirm

ssl = get_satchmo_setting('SSL', default_value=False)

urlpatterns = patterns('',
     url(r'^$', payment.modules.paypal.views.pay_ship_info, {'SSL': ssl}, 'PAYPAL_satchmo_checkout-step2'),
     url(r'^confirm/$', payment.modules.paypal.views.confirm_info, {'SSL': ssl}, 'PAYPAL_satchmo_checkout-step3'),
     url(r'^success/$', payment.modules.paypal.views.success, {'SSL': ssl}, 'PAYPAL_satchmo_checkout-success'),
     url(r'^ipn/$', payment.modules.paypal.views.ipn, {'SSL': ssl}, 'PAYPAL_satchmo_checkout-ipn'),
     url(r'^confirmorder/$', payment.views.confirm.confirm_free_order,
         {'SSL' : ssl, 'key' : 'PAYPAL'}, 'PAYPAL_satchmo_checkout_free-confirm')
)
