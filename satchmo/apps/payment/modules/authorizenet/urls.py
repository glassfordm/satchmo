from satchmo_utils.django_compat import patterns, url
from satchmo_store.shop.satchmo_settings import get_satchmo_setting

ssl = get_satchmo_setting('SSL', default_value=False)

import payment.modules.authorizenet.views
import payment.views.checkout

urlpatterns = patterns('',
     url(r'^$', payment.modules.authorizenet.views.pay_ship_info, {'SSL':ssl}, 'AUTHORIZENET_satchmo_checkout-step2'),
     url(r'^confirm/$', payment.modules.authorizenet.views.confirm_info, {'SSL':ssl}, 'AUTHORIZENET_satchmo_checkout-step3'),
     url(r'^success/$', payment.views.checkout.success, {'SSL':ssl}, 'AUTHORIZENET_satchmo_checkout-success'),
)
