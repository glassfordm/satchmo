from satchmo_utils.django_compat import patterns, url
from satchmo_store.shop.satchmo_settings import get_satchmo_setting

import modules.purchaseorder.views
import views.checkout

ssl = get_satchmo_setting('SSL', default_value=False)

urlpatterns = patterns('payment',
    url(r'^$', modules.purchaseorder.views.pay_ship_info,
        {'SSL':ssl}, 'PURCHASEORDER_satchmo_checkout-step2'),

    url(r'^confirm/$', modules.purchaseorder.views.confirm_info,
        {'SSL':ssl}, 'PURCHASEORDER_satchmo_checkout-step3'),

    url(r'^success/$', views.checkout.success,
        {'SSL':ssl}, 'PURCHASEORDER_satchmo_checkout-success'),
)
